﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This zoom camera along z axies
/// </summary>
public class MouseWheelZoomCamera : MonoBehaviour {
	public GameObject moveObj;
	public float closestZoomZ;
	public float futherestZoomZ;
	public float zoomSpd = 10;

	void Update () {
		float scrollingInput = Input.GetAxis("Mouse ScrollWheel");
		if(scrollingInput > 0) {
			if(moveObj.transform.localPosition.z < closestZoomZ) {
				moveObj.transform.Translate(0,0,scrollingInput*zoomSpd, Space.Self);
			}
		}
		else if(scrollingInput < 0) {
			if(moveObj.transform.localPosition.z > futherestZoomZ) {
				moveObj.transform.Translate(0,0,scrollingInput*zoomSpd, Space.Self);
			}
		}
	}
}
