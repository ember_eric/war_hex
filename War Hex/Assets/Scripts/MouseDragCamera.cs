using UnityEngine;

public class MouseDragCamera : MonoBehaviour {
    public GameObject moveObj;
    public float dragSpeed = 10.0f;
    bool isDragging = false;
    Vector3 lastMousePos;
    Vector3 move;
    private void Update() {
        if(Input.GetMouseButtonDown(2)) {
            isDragging = true;
            lastMousePos = Camera.main.ScreenToViewportPoint(Input.mousePosition);
        }

        if(Input.GetMouseButtonUp(2)) {
            isDragging = false;
        }

        if(isDragging) {
            if(Camera.main.ScreenToViewportPoint(Input.mousePosition) != lastMousePos) {
                move = Camera.main.ScreenToViewportPoint(Input.mousePosition) - lastMousePos;
                lastMousePos = Camera.main.ScreenToViewportPoint(Input.mousePosition);
                moveObj.transform.Translate(-move.x*dragSpeed, 0 ,-move.y*dragSpeed, Space.World); //Only pan along x and z aixes
            }
        }
    }
}